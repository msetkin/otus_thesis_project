#!/usr/bin/env python3
import json
from confluent_kafka import Producer
from sseclient import SSEClient as EventSource

wikipedia_url = 'https://stream.wikimedia.org/v2/stream/recentchange'

p = Producer({'bootstrap.servers': 'kafka:9092'})

try:
    for event in EventSource(wikipedia_url):
        if event.event == 'message':
            try:
                change = json.loads(event.data)
            except ValueError:
                pass
            else:
                print('change by: {user}'.format(**change))
                p.produce('wikiedit', key='{user}'.format(**change), value=event.data)
                p.poll(1)
            
except KeyboardInterrupt:
    pass

p.flush(10)
