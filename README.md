# otus_thesis_project

This is my Otus Data Engineer (https://otus.ru/lessons/data-engineer/) thesis 
where I would like to apply gained knowledge building some elements of a Data Lake
and get to know a few key tools of a data engineering world like Kafka, NiFi, Minio.

In this work I would like to test S3 compatible object store as an alternative to 
Hadoop when it comes to a raw data layer of data lakes, as well as tools of loading
data into Minio. As a data source I picked up a publicly available Wikipedia's
Recent Changes feed  (https://www.mediawiki.org/wiki/Manual:RCFeed).

End-to-end data processing pipeline of this work is as following:
```Wikipedia API -> Python script -> Kafka -> Nifi -> Minio (S3) -> Hive (ext. table)```

This repo has the following structure:
```bash
<root folder>
├── docker --configs and other files for docker images creation using docker-compose
│   ├── kafka
│   │   └── ...
│   └── minio-hadoop-presto
│       └── ... 
├── nifi
│   └── Kafka2Minio.xml --template xml for NiFi process group to load data from Kafka to S3
├── python 
│   ├── push_wikiedit_to_kafka.py --python script to load data from Wiki API to Kafka
│   └── requirements.txt --libs that are used
├── sql
│   ├── analytical_queries_hive.sql --Examples of analytical queries in Hive over loaded data
│   ├── create_external_table_hive.sql --DDL for creation of an external table in Hive over S3 data
│   └── create_partitions_hive.sql  --DDL for creation of a partition in Hive
├── img --images for this readme
└── README.md --this file
```

Roll out instruction manual
1. Clone this repo to your own PC:
```bash
git clone https://gitlab.com/msetkin/otus_thesis_project.git
```

2. Build and run Kafka container:
```bash
cd <root folder>/docker/kafka
docker-compose up
```

3. Build and run Minio and Hadoop/Hive containers:
```bash
cd <root folder>/docker/minio-hadoop-presto
docker-compose up
```

You can view the Minio Browser at http://127.0.0.1:9000/
   MINIO_ACCESS_KEY: V42FCGRVMK24JJ8DHUYG
   MINIO_SECRET_KEY: bKhWxVF3kQoLY9kFmt91l+tDrEoZjqnWXzY9Eza
There are already a few buckets created in Minio, including raw-wikiedit that we will 
use later.

4. Create an external table in Hive and two partitions in Hive for the data that
is already in the bucket:
```bash
docker cp sql/create_external_table_hive.sql hadoop-master:/tmp
docker cp sql/create_partitions_hive.sql hadoop-master:/tmp
docker exec hadoop-master hive --hiveconf hive.root.logger=DEBUG,DRFA -f /tmp/create_external_table_hive.sql
docker exec hadoop-master hive --hiveconf hive.root.logger=DEBUG,DRFA -f /tmp/create_partitions_hive.sql
```

Check that a combination "Minio/Hive" works:
```bash
docker exec hadoop-master hive --hiveconf hive.root.logger=DEBUG,DRFA -e "select count(1) as cnt from wiki where load_date='2020-06-20';"
```

5. Install and start NiFi using instructions from https://nifi.apache.org/docs/nifi-docs/html/getting-started.html
(Do not forget to change a port in <NiFi download root folder>/conf/nifi.properties:
    nifi.web.http.port=8085
as the default NiFi port 9000 conflicts with the one from Minio)

Enter NiFi by opening http://localhost:8085 in the browser and upload template 
from the project's <root folder>/nifi/Kafka2Minio.xml

Run all NiFi processors in the imported 'Kafka2Minio' process group (except 'wiki-dump' PutFile,
as it was used for debugging purposes).
  
6. Choose your Python's 3.6 environment working kernel (in my case it was virtualenv 3.6.8),
install necessarry libraries:
```bash
pip install <root folder>/python/requirements.txt
```

run the script pushing data from Wiki API to Kafka:
```bash
python <root folder>/python/push_wikiedit_to_kafka.py
```

7. Before runnning queries at the first time, for the current date you need to create a pertition in Hive manually:
```bash
docker exec hadoop-master hive --hiveconf hive.root.logger=DEBUG,DRFA -e "ALTER TABLE wiki add PARTITION (load_date='<current date in YYYY-MM-DD>') LOCATION 's3a://raw-wikiedit/load_date=<current date in YYYY-MM-DD>/';"
```

8. Presto
It turns out that Presto does not work well with nested structures and fields which names are reserved words as in this case data. To fix that, we would need to create a view in Presto that is cut to only usable fields:
```bash
docker cp sql/create_view_presto.sql presto:/tmp
docker exec -it presto presto-cli --file /tmp/create_view_presto.sql 
```

9. Now it's time to view the result with a certain BI tool. I use Metabase (https://www.metabase.com/). To install it, run the command:
```bash
docker run -d -p 3000:3000 --name metabase metabase/metabase 
```

After that, you'll need to create a network between two docker containers so that they "see" each other:
```bash
sudo docker network create myNetwork
sudo docker network connect myNetwork presto
sudo docker network connect myNetwork metabase 
```

Next, go to the Metabase UI (http://localhost:3000/) and as an admin establish connection (http://localhost:3000/admin/databases) to Presto using parameters:
database type: presto
name: any
host: presto
port: 8080
database name: minio
database username: any
database password: any

And finally, you'll need to create a dashboard with some aggregated statistics over the data.
I chose 4 tabluar views the queries for which that can be found in "SQL/metabase_queries.sql" file.
The result of such a dashboard is shown at the picture below:
<img src="./img/metabase.png">

10. As an alternative to Metabase, to analyse data in Presto you can use SQL client DBeaver (https://dbeaver.io/) and SQL language.
In this case, for the DB connection 'PrestoDB' use the following settings:
jdbc url: jdbc:presto://localhost:8080/minio
username: any
password: any
The example queries for analysis can be found at the same folder "SQL/metabase_queries.sql".
 

11. Make sure the data is being delivered end-to-end in the following points:
- Python - look at the script's output
- NiFi - check the processors' statistics
- Minio - use web-interface
- Hive - you can run some analytical queries from <root folder>/sql/analytical_queries_hive.sql
and pay attention to `count` column changes over time to make sure the data is loaded in real-time.
- DBeaver/Metabase - you can write/build your own analytical reports using these tools.


