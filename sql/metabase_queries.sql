--popular pages per day
  select
         title,
	 count() as cnt
    from
	 default.wiki_v
   where load_date = {{load_date}}
group by
         title
	 order by cnt desc
   limit 10


--Top editors per day
  select user,
         bot,
         count(1) as cnt
    from default.wiki_v 
   where load_date={{load_date}} 
group by user,
         bot
order by cnt desc
   limit 10


--Who edits what?
 select title, 
        user,
        count(1) as cnt 
   from default.wiki_v
  where load_date={{load_date}}
group by title,
         user
order by cnt desc
   limit 10

--Wiki edits per day
  select load_date,
         count(1) as cnt
    from default.wiki_v
   where load_date = {{load_date}}
group by load_date
