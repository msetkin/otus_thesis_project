--total number of loaded records (wiki edits) per day
select count(1) as cnt from wiki where load_date='2020-06-20'

--top 10 edited wiki page URIs per day
select meta.uri, count(1) as cnt from wiki where load_date='2020-06-20' group by meta.uri order by cnt desc limit 10;

--top 10 wiki domains with most edits per day
select meta.domain, count(1) as cnt from wiki where load_date='2020-06-20' group by meta.domain order by cnt desc limit 10;

--top 10 edited wiki page names per day
select title, count(1) as cnt from wiki where load_date='2020-06-20' group by title order by cnt desc limit 10;

--top 10 wiki editors per day
select user, bot, count(1) as cnt from wiki where load_date='2020-06-20' group by user, bot order by cnt desc limit 10;
