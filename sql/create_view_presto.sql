create or replace view "minio"."default"."wiki_v" as 
select 
	id, 
	type, 
	namespace, 
	title,
	comment, 
	timestamp, 
	user,
	bot,
	minor, 
	length,
	revision,
	server_url
	server_name,
	server_script_path,
	wiki,
	parsedcomment,
	load_date
from minio.default.wiki;
