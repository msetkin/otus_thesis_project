create external table wiki(
meta struct<uri:string,
            request_id:string,
            id:string,
            dt:timestamp,
            domain:string,
            stream:string,
            topic:string,
            `partition`:int,
            offset:bigint>,
id bigint,
type string,
namespace int,
title string,
comment string,
`timestamp` timestamp,
`user` string,
bot boolean,
minor boolean,
length struct<old:int,new:int>,
revision struct<old:int,new:int>,
server_url string,
server_name string,
server_script_path string,
wiki string,
parsedcomment string) 
PARTITIONED BY (load_date string)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
STORED AS TEXTFILE location 's3a://raw-wikiedit/';
--TBLPROPERTIES ("hive.serialization.extend.nesting.levels"="true", "hive.serialization.extend.additional.nesting.levels"="true")
